﻿using System;
using System.Globalization;
using System.Linq;
using AutoFixture;
using Castle.Core.Internal;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _controller;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var partnerRepositoryMock = new MockRepositoryBuilder<Partner>()
                .EnableGetByIdAsync()
                .EnableUpdateAsync()
                .SetData(FakeDataFactory.Partners)
                .Build();
            _controller = new PartnersController(partnerRepositoryMock.Object);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WithAbsentId_NotFound()
        {
            //Arrange
            var (id, _) = InitParam(IdKind.NotFound);
            //Act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, null);
            //Assert //FluentAssertions
            res.Should().BeOfType(typeof(NotFoundResult));
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ActivePartner_False()
        {
            //Arrange
            var (id, _) = InitParam(IdKind.NotActive);
            //Act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, null);
            //Assert //FluentAssertions
            res.Should().BeOfType(typeof(BadRequestObjectResult));
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_NumberIssuedPromoCodesIs0()
        {
            //Arrange
            var (id, request) = InitParam(IdKind.SetLimit);
            
            //Act
            var partnerBefore = await _controller.GetPartnerByIdAsync(id);   
            var limitBefore = partnerBefore.PartnerLimits
                .FirstOrDefault(x => x.CancelDate.IsNullOrEmpty());
            
            await _controller.SetPartnerPromoCodeLimitAsync(id, request);
            
            var partnerAfter = await _controller.GetPartnerByIdAsync(id);
            
            //Assert //FluentAssertions
            if (limitBefore != default && limitBefore.Limit <= partnerBefore.NumberIssuedPromoCodes)
                partnerBefore.NumberIssuedPromoCodes.Should().Be(partnerAfter.NumberIssuedPromoCodes);
            else
                partnerAfter.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CancelDateForPrevLimit_IsNotEmpty()
        {
            //Arrange
            var (id, request) = InitParam(IdKind.PrevLimitIsOff);
            
            //Act
            var partnerBefore = await _controller.GetPartnerByIdAsync(id);    
            await _controller.SetPartnerPromoCodeLimitAsync(id, request);
            var partnerAfter = await _controller.GetPartnerByIdAsync(id);
            var limitBefore = partnerBefore.PartnerLimits
                .FirstOrDefault(x => x.CancelDate.IsNullOrEmpty());
            
            //Assert //FluentAssertions
            partnerAfter.PartnerLimits.First(x => x.Id == limitBefore?.Id)
                .CancelDate.Should().NotBeNullOrWhiteSpace();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_BadLimit_BadRequest()
        {
            //Arrange
            var (id, request) = InitParam(IdKind.BadLimit);
            //Act
            var res = await _controller.SetPartnerPromoCodeLimitAsync(id, request);
            //Assert
            Assert.True(res is BadRequestObjectResult);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_SavedToDb()
        {
            //Arrange
            var (id, request) = InitParam(IdKind.SavedLimit);
            
            //Act
            var partnerBefore = await _controller.GetPartnerByIdAsync(id);   
            await _controller.SetPartnerPromoCodeLimitAsync(id, request);
            var partnerAfter = await _controller.GetPartnerByIdAsync(id);
            var limits = partnerAfter.PartnerLimits
                .Where(x => 
                    partnerBefore.PartnerLimits.All(y => y.Id != x.Id)).ToArray(); 
            
            //Assert //FluentAssertions
            limits.Should().ContainSingle();
            limits[0].Limit.Should().Be(request.Limit);
            DateTime.Parse(limits[0].EndDate, CultureInfo.InvariantCulture).Should().Be(request.EndDate);
            limits[0].CancelDate.Should().BeNull();
        }
        

        private static (Guid, SetPartnerPromoCodeLimitRequest) InitParam(IdKind idKind)
        {
            Guid id;
            switch (idKind)
            {
                case IdKind.NotFound:
                    var autoFixture = new Fixture();
                    id = autoFixture.Create<Guid>();
                    break;
                case IdKind.NotActive:
                    id = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");
                    break;
                case IdKind.SetLimit: case IdKind.PrevLimitIsOff: case IdKind.SavedLimit:
                    id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
                    break;
                case IdKind.BadLimit:
                    return (
                        Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                        new SetPartnerPromoCodeLimitRequest()
                        {
                            EndDate = DateTime.Parse("2099-01-01 23:59:59", CultureInfo.InvariantCulture),
                            Limit = 0
                        });
                default:
                    id = default;
                    break;
            }

            return (id, new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Parse("2099-01-01 23:59:59", CultureInfo.InvariantCulture),
                Limit = 2
            });
        }
        
        private enum IdKind
        {
            NotFound,
            NotActive,
            SetLimit,
            PrevLimitIsOff,
            BadLimit,
            SavedLimit
        }
    }
}